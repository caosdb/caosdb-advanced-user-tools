# -*- mode: snippet -*-
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# name: Property inside RecordType
# key: prop
# expand-env: ((yas-indent-line 'fixed))
# --
${1:property_name}:
  datatype: ${2:$$(yas-choose-value '("BOOLEAN"
                                      "DATETIME"
                                      "DOUBLE"
                                      "FILE"
                                      "INTEGER"
                                      "LIST"
                                      "REFERENCE"
                                      "TEXT"))}
  description: ${3:description text}
$0