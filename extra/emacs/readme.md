# Emacs extras #

This directory contains extra utils for use with Emacs.

## Snippets ##

if you copy the contents of the `snippets` directory to your `~/.emacs.d/snippets/`, the following
*yasnippet* snippets will become available:

- yaml-mode:
  - `RT`: Insert a new RecordType, with inheritance and properties sections.
  - `prop`: Insert a new Property into a RecordType, with datatype and description.
