JSON schema from data model
===========================

Sometimes you may want to have a `json schema <https://json-schema.org>`_ which describes a
LinkAhead data model, for example for the automatic generation of user interfaces with third-party
tools like `rjsf <https://rjsf-team.github.io/react-jsonschema-form/docs/>`_.

For this use case, look at the documentation of the `caosadvancedtools.json_schema_exporter` module.
