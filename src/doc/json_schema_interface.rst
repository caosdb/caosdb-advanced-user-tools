Defining datamodels with a JSON schema specification
====================================================

TODO, see https://gitlab.com/caosdb/caosdb-advanced-user-tools/-/issues/42

Further information
###################

Pattern Properties
%%%%%%%%%%%%%%%%%%

The JSON-schema parser has rudimentary support for ``patternProperties``. Since
their names (only the pattern that their names will suffice) are not known a
priori, we create RecordTypes for all pattern properties. The names of these
RecordTypes are created from their parent element's name by appending the string
``"Entry"`` and possibly a number if there are more than one pattern properties
for one parent.

All the RecordTypes created for pattern properties have at least an obligatory
``__matched_pattern`` property which will -- as the name suggests -- store the
matched pattern of an actual data entry.

.. note::

   The ``__matched_pattern`` property is added automatically to your datamodel
   as soon as there is at least one pattern property in your JSON schema. So be
   sure that you don't happen to have an entity with exactly this name in your
   database.

E.g., a json schema with

.. code-block:: json

   "dataset": {
     "patternProperties": {
        "^[0-9]{4,4}": {
            "type": "boolean"
        },
        "^[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}": {
            "type": "object",
            "properties": {
                "date_id": {
                    "$ref": "#/definitions/uuid"
                }
            }
        }
     }
   }

Would result in a ``Dataset`` RecordType that has the two properties
``DatasetEntry_1`` and ``DatasetEntry_2`` (as always, name can be overwritten
explicitly by specifying the ``title`` property), referencing corresponding
``DatasetEntry_1`` and ``DatasetEntry_2`` Records.

Apart from the aforementioned ``__matched_pattern`` property, ``DatasetEntry_1``
also has the ``DatasetEntry_1_value`` property with datatype ``BOOLEAN``, that
stores the actual value. In turn, ``DatasetEntry_2`` is of ``type: object`` and
is treated like any other RecordType. Consequently, it has, appart from the
``__matched_pattern`` property, a ``date_id`` property as specified in its
``properties``.

Array entries without ``items`` specification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

JSON schema allows for properties of ``type: array`` without the ``items``
specification that consequently can be arrays of any (and of mixed) types. While
this is in general problematic when specifying a data model, sometimes these
properties cannot be specified further, e.g., when you're using an external
schema that you cannot change.

These properties can still be added to your datamodel by specifying their types
explicitly in a dictionary or, alternatively, they can be ignored. See the
``types_for_missing_array_items`` and ``ignore_unspecified_array_items``
parameters of ``models.parser.JsonSchemaParser``, respectively, for more
information.
