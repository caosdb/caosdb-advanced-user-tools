Welcome to caosadvancedtools' documentation!
============================================

Welcome to the advanced Python tools for CaosDB!


This documentation helps you to :doc:`get started<README_SETUP>`, explains the most important
:doc:`concepts<concepts>` and offers a range of deep dives into specific sub-topics.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Getting started <README_SETUP>
   Concepts <concepts>
   The Caosdb Crawler <crawler>
   YAML data model specification <yaml_interface>
   Specifying a datamodel with JSON schema <json_schema_interface>
   Convert a data model into a json schema <json_schema_exporter>
   Conversion between XLSX, JSON and LinkAhead Entities <table-json-conversion/specs>
   Other utilities <utilities>
   _apidoc/modules
   Related Projects <related_projects/index>
   Back to overview <https://docs.indiscale.com/>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
