#!/usr/bin/env python3
"""
replaces git branch names with the newest commit hash using gitlab api
"""
import argparse

import requests


_REPOS = {
    "SERVER": "https://gitlab.indiscale.com/api/v4/projects/100",
    "WEBUI": "https://gitlab.indiscale.com/api/v4/projects/98",
    "PYLIB": "https://gitlab.indiscale.com/api/v4/projects/97",
    "MYSQLBACKEND": "https://gitlab.indiscale.com/api/v4/projects/101",
    "PYINT": "https://gitlab.indiscale.com/api/v4/projects/99",
    "CPPLIB": "https://gitlab.indiscale.com/api/v4/projects/107",
    "CPPINT": "https://gitlab.indiscale.com/api/v4/projects/111",
    "ADVANCEDUSERTOOLS": "https://gitlab.indiscale.com/api/v4/projects/104"
}


def get_remote(repository):
    return _REPOS[repository]


def ref_to_commit(repository, reference):
    remote = get_remote(repository)
    r = requests.get(remote+"/repository/branches/"+reference).json()

    if "name" in r:
        return r["commit"]["short_id"]

    return reference


def define_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("repository")
    parser.add_argument("reference")

    return parser


if __name__ == "__main__":
    parser = define_parser()
    args = parser.parse_args()
    ret = ref_to_commit(repository=args.repository, reference=args.reference)
    print(ret)
