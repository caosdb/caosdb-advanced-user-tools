#!/usr/bin/env python3
"""
Exit with error code 2 if the branch does not exist.
"""
import sys
import argparse
import requests
from ref_to_commit import get_remote


def branch_exists(repository, branch):
    remote = get_remote(repository)
    resp = requests.get(remote+"/repository/branches/"+branch).json()
    return "message" not in resp


def define_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("repository")
    parser.add_argument("branchname")

    return parser


if __name__ == "__main__":
    parser = define_parser()
    args = parser.parse_args()
    ret = branch_exists(repository=args.repository, branch=args.branchname)
    if ret is False:
        print("branch does not exist.")
        sys.exit(2)
    else:
        print("branch exists.")
