import unittest

import linkahead as db
from caosadvancedtools.models.data_model import DataModel
from caosadvancedtools.models.parser import parse_model_from_string


class DataModelTest(unittest.TestCase):

    def test_collecting(self):
        maintained = {"one": db.RecordType(name="TestRecord").add_property(
            name="testproperty"),
                      "two": db.Property(name="testproperty", datatype=db.INTEGER)}
        dm = DataModel(maintained.values())
        col = dm.collect_entities()
        names = [e.name for e in col]
        assert "TestRecord" in names
        assert "testproperty" in names

    def test_sync_ids_by_name(self):
        container = db.Container().extend([db.RecordType(name="TestRecord"),
                                           db.RecordType(name="TestRecord2"),
                                           ])

        # assign negative ids
        container.to_xml()
        l1 = DataModel(container)

        rt = db.RecordType(name="TestRecord")
        rt.id = 1002
        rt2 = db.RecordType(name="TestRecordNonono")
        rt2.id = 1000
        l2 = [rt2, rt]
        DataModel.sync_ids_by_name(l1, l2)
        assert l1["TestRecord"].id == rt.id
        assert l1["TestRecord2"].id < 0

    def test_get_deep(self):
        model_recursive_str = """
RT1:
  description: some description
  obligatory_properties:
    RT1:
        """
        model_recursive = parse_model_from_string(model_recursive_str)
        prop1 = model_recursive["RT1"].get_property("RT1")

        assert prop1.datatype is not None
        assert prop1.datatype == "RT1"

        # TODO The next line actually changes model_recursive in place, is this OK?
        RT1 = model_recursive.get_deep("RT1")
        assert model_recursive["RT1"] == RT1

        model_unresolved_str = """
RT1:
  description: some description
  obligatory_properties:
    unresolved:
        """
        model_unresolved = parse_model_from_string(model_unresolved_str)
        rt1_unresolved = model_unresolved["RT1"]
        prop_unresolved = model_unresolved.get_deep("unresolved")
        assert prop_unresolved.datatype is None
        rt1_deep = model_unresolved.get_deep("RT1")
        assert rt1_deep == rt1_unresolved
        assert rt1_deep is rt1_unresolved

        model_double_property = """
p1:
  description: Hello world
  datatype: TEXT
RT1:
  recommended_properties:
    p1:
RT2:
  recommended_properties:
    RT1:
    p1:
"""
        model_unresolved = parse_model_from_string(model_double_property)
        rt2_deep = model_unresolved.get_deep("RT2")
        p1 = rt2_deep.get_property("p1")
        assert p1.datatype == "TEXT"
        assert p1.description == "Hello world"
