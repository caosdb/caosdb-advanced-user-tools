#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (C) 2020 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import datetime
import os
import unittest
from functools import partial
from tempfile import NamedTemporaryFile

import numpy as np
import pandas as pd
import pytest
from caosadvancedtools.datainconsistency import DataInconsistencyError
from caosadvancedtools.table_importer import (CSVImporter, TableImporter,
                                              TSVImporter, XLSImporter,
                                              assure_name_format,
                                              check_reference_field,
                                              date_converter,
                                              datetime_converter,
                                              incomplete_date_converter,
                                              string_in_list,
                                              win_path_converter,
                                              win_path_list_converter,
                                              yes_no_converter)

from test_utils import BaseMockUpTest

# For testing the table importer
IMPORTER_KWARGS = dict(
    converters={'c': float, 'd': yes_no_converter, 'x': float},  # x does not exist
    datatypes={'a': str, 'b': int, 'float': float, 'x': int},  # x does not exist
    obligatory_columns=['a', 'b'], unique_keys=[('a', 'b')],
    existing_columns=['e'],
)
VALID_DF = pd.DataFrame(
    [['a', 1, 2.0, 'yes', np.nan]], columns=['a', 'b', 'c', 'd', 'e'])


class ConverterTest(unittest.TestCase):
    def test_yes_no(self):
        self.assertTrue(yes_no_converter("YES"))
        self.assertTrue(yes_no_converter("Yes"))
        self.assertTrue(yes_no_converter("yes"))
        self.assertTrue(not yes_no_converter("No"))
        self.assertTrue(not yes_no_converter("no"))
        self.assertRaises(ValueError, yes_no_converter, "nope")
        self.assertRaises(ValueError, yes_no_converter, "FALSE")
        self.assertRaises(ValueError, yes_no_converter, "TRUE")
        self.assertRaises(ValueError, yes_no_converter, "True")
        self.assertRaises(ValueError, yes_no_converter, "true")

    def test_string_in_list(self):
        self.assertEqual("false", string_in_list("false",
                                                 ["FALSE", "TRUE"]))
        self.assertEqual("FALSE", string_in_list("FALSE",
                                                 ["FALSE", "TRUE"], False))
        self.assertRaises(ValueError, string_in_list, "FALSE", [])
        self.assertRaises(ValueError, string_in_list, "FALSE", ["fals"])
        self.assertRaises(ValueError, string_in_list,
                          "FALSE", ["false"], False)

    def test_assure_name_format(self):
        self.assertEqual(assure_name_format("Müstermann, Max"),
                         "Müstermann, Max")
        self.assertRaises(ValueError, assure_name_format, "Max Mustermann")

    def test_winpath(self):
        self.assertRaises(ValueError, win_path_converter, "/hallo/python")
        self.assertEqual(win_path_converter(r"\this\computer"),
                         "/this/computer")
        self.assertEqual(win_path_list_converter(r"\this\computer"),
                         ["/this/computer"])
        self.assertEqual(win_path_list_converter(
            r"\this\computer,\this\computer"),
            ["/this/computer", "/this/computer"])

    def test_datetime(self):
        test_file = os.path.join(os.path.dirname(__file__), "date.xlsx")
        importer = XLSImporter(converters={'d': datetime_converter,
                                           }, obligatory_columns=['d'])

        xls_file = pd.io.excel.ExcelFile(test_file)
        df = xls_file.parse()
        df = importer.read_xls(test_file)
        assert df.shape[0] == 2
        # TODO datatypes are different; fix it
        assert df.d.iloc[0] == datetime.datetime(1980, 12, 31, 13, 24, 23)

    def test_date_xlsx(self):
        """Test with .xlsx in order to check openpyxl engine."""
        test_file = os.path.join(os.path.dirname(__file__), "date.xlsx")
        importer = XLSImporter(converters={'a': date_converter,
                                           'b': date_converter,
                                           'c': partial(date_converter,
                                                        fmt="%d.%m.%y")
                                           }, obligatory_columns=['a'])

        xls_file = pd.io.excel.ExcelFile(test_file)
        df = xls_file.parse()
        df = importer.read_xls(test_file)
        assert df.shape[0] == 2
        assert df.a.iloc[0] == df.b.iloc[0] == df.c.iloc[0]

    def test_date_xls(self):
        """Test with .xls in order to check xlrd engine."""
        test_file = os.path.join(os.path.dirname(__file__), "date.xls")
        importer = XLSImporter(converters={'a': date_converter,
                                           'b': date_converter,
                                           'c': partial(date_converter,
                                                        fmt="%d.%m.%y")
                                           }, obligatory_columns=['a'])

        xls_file = pd.io.excel.ExcelFile(test_file)
        df = xls_file.parse()
        df = importer.read_xls(test_file)
        assert df.shape[0] == 2
        assert df.a.iloc[0] == df.b.iloc[0] == df.c.iloc[0]

    def test_inc_date(self):
        incomplete_date_converter("2020", fmts={"%Y": "%Y"}) == "2020"
        incomplete_date_converter("02/2020",
                                  fmts={"%Y": "%Y", "%Y-%m": "%m/%Y"}
                                  ) == "2020-02"
        incomplete_date_converter("02/02/2020",
                                  fmts={"%Y": "%Y", "%Y-%m": "%m/%Y",
                                        "%Y-%m-%d": "%d/%m/%Y"}
                                  ) == "2020-02-02"
        incomplete_date_converter("2020",
                                  fmts={"%Y": "%Y", "%Y-%m": "%m/%Y",
                                        "%Y-%m-%d": "%d/%m/%Y"}
                                  ) == "2020"
        self.assertRaises(RuntimeError,
                          incomplete_date_converter,
                          "2020e",
                          fmts={"%Y": "%Y"})


class TableImporterTest(unittest.TestCase):
    def setUp(self):
        self.importer_kwargs = IMPORTER_KWARGS
        self.valid_df = VALID_DF

    def test_missing_col(self):
        # check missing from existing
        df = pd.DataFrame(columns=['a', 'b'])
        importer = TableImporter(**self.importer_kwargs)
        with pytest.raises(DataInconsistencyError) as die:
            importer.check_columns(df)
        assert "Column 'e' missing" in str(die.value)
        # check valid
        importer.check_columns(self.valid_df)

    def test_missing_val(self):
        importer = TableImporter(**self.importer_kwargs)
        # check valid
        importer.check_missing(self.valid_df)
        # check invalid
        df = pd.DataFrame([[None, np.nan, 2.0, 'yes'],
                           [None, 1, 2.0, 'yes'],
                           ['a', np.nan, 2.0, 'yes'],
                           ['b', 5, 3.0, 'no']],
                          columns=['a', 'b', 'c', 'd'])
        df_new = importer.check_missing(df)
        self.assertEqual(df_new.shape[0], 1)
        self.assertEqual(df_new.shape[1], 4)
        self.assertEqual(df_new.iloc[0].b, 5)

        # check that missing array-valued fields are detected correctly:
        df = pd.DataFrame([[[None, None], 4, 2.0, 'yes'],
                           ['b', 5, 3.0, 'no']],
                          columns=['a', 'b', 'c', 'd'])
        df_new = importer.check_missing(df)
        self.assertEqual(df_new.shape[0], 1)
        self.assertEqual(df_new.shape[1], 4)
        self.assertEqual(df_new.iloc[0].b, 5)

    def test_wrong_datatype(self):
        importer = TableImporter(**self.importer_kwargs)
        df = pd.DataFrame([[1234, 0, 2.0, 3, 'yes'],
                           [5678, 1, 2.0, 3, 'yes']],
                          columns=['a', 'b', 'c', 'float', 'd'])
        # wrong datatypes before
        assert df["a"].dtype != pd.StringDtype
        assert df["float"].dtype != float
        # strict = False by default, so this shouldn't raise an error
        importer.check_datatype(df)
        # The types should be correct now.
        assert df["a"].dtype == pd.StringDtype
        assert df["float"].dtype == float

        # Resetting `df` since check_datatype may change datatypes
        df = pd.DataFrame([[None, 0, 2.0, 'yes'],
                           [5, 1, 2.0, 'yes']],
                          columns=['a', 'b', 'c', 'd'])
        # strict=True, so number in str column raises an error
        self.assertRaises(DataInconsistencyError, importer.check_datatype, df, None, True)

        df = pd.DataFrame([[0],
                           [1]],
                          columns=['float'])
        # strict=True, so int in float column raises an error
        self.assertRaises(DataInconsistencyError, importer.check_datatype, df, None, True)

        # This is always wrong (float in int column)
        df = pd.DataFrame([[None, np.nan, 2.0, 'yes'],
                           [5, 1.7, 2.0, 'yes']],
                          columns=['a', 'b', 'c', 'd'])
        self.assertRaises(DataInconsistencyError, importer.check_datatype, df, None, False)

    def test_unique(self):
        importer = TableImporter(**self.importer_kwargs)
        importer.check_missing(self.valid_df)
        df = pd.DataFrame([['b', 5, 3.0, 'no'], ['b', 5, 3.0, 'no']],
                          columns=['a', 'b', 'c', 'd'])
        df_new = importer.check_unique(df)
        self.assertEqual(df_new.shape[0], 1)


def test_check_dataframe_existing_obligatory_columns(caplog):
    """Needs caplog so remove from above class."""
    # stricter test case; column 'a' must exist and have a value
    strict_kwargs = IMPORTER_KWARGS.copy()
    strict_kwargs["existing_columns"].append('a')

    importer = TableImporter(**strict_kwargs)

    # the valid df is still valid, since 'a' has a value
    importer.check_dataframe(VALID_DF)

    # Now 'a' doesn't
    df_missing_a = pd.DataFrame(
        [[np.nan, 1, 2.0, 'yes', 'e']], columns=['a', 'b', 'c', 'd', 'e'])

    new_df = importer.check_dataframe(df_missing_a)
    # Column is removed and a warning is in the logger:
    assert new_df.shape[0] == 0
    assert "Required information is missing (a) in 1. row" in caplog.text

    df_missing_c = pd.DataFrame(
        [['a', 1, 'yes', np.nan]], columns=['a', 'b', 'd', 'e'])
    new_df = importer.check_dataframe(df_missing_c)
    assert new_df.shape[0] == 1
    assert new_df.shape[1] == 4

    caplog.clear()


class XLSImporterTest(TableImporterTest):
    def test_full(self):
        """ test full run with example data """
        tmp = NamedTemporaryFile(delete=False, suffix=".xlsx")
        tmp.close()
        self.valid_df.to_excel(tmp.name)
        importer = XLSImporter(**self.importer_kwargs)
        importer.read_file(tmp.name)

    def test_raise(self):
        importer = XLSImporter(**self.importer_kwargs)
        tmp = NamedTemporaryFile(delete=False, suffix=".lol")
        tmp.close()
        self.assertRaises(DataInconsistencyError, importer.read_xls,
                          tmp.name)

    def test_datatypes(self):
        """Test datataypes in columns."""
        importer = XLSImporter(converters={},
                               obligatory_columns=["float_as_float"],
                               datatypes={
                                   "float_as_float": float,
                                   "int_as_float": float,
                                   "int_as_int": int,
        }
        )
        df = importer.read_xls(os.path.join(
            os.path.dirname(__file__), "data", "datatypes.xlsx"))
        assert np.issubdtype(df.loc[0, "int_as_float"], float)


class CSVImporterTest(TableImporterTest):
    def test_full(self):
        """ test full run with example data """
        tmp = NamedTemporaryFile(delete=False, suffix=".csv")
        tmp.close()
        self.valid_df.to_csv(tmp.name)
        importer = CSVImporter(**self.importer_kwargs)
        importer.read_file(tmp.name)

    def test_with_generous_datatypes(self):
        """Same as above but check that values are converted as expected."""
        tmp = NamedTemporaryFile(delete=False, suffix=".csv")
        tmp.close()
        self.valid_df.to_csv(tmp.name)
        # Copy and use float for columns with integer values, string for columns
        # with numeric values
        kwargs = self.importer_kwargs.copy()
        kwargs["datatypes"] = {
            'a': str,
            'b': float,
            'c': str
        }
        importer = CSVImporter(**kwargs)
        importer.read_file(tmp.name)

        kwargs["datatypes"] = {
            'a': str,
            'b': str,
            'c': str
        }
        importer = CSVImporter(**kwargs)
        importer.read_file(tmp.name)

    def test_gaps_in_int_column(self):
        """Test for
        https://gitlab.com/linkahead/linkahead-advanced-user-tools/-/issues/62:
        Datatype confusion when encountering empty values in integer columns.

        """
        tmpfile = NamedTemporaryFile(delete=False, suffix=".csv")
        with open(tmpfile.name, 'w') as tmp:
            tmp.write(
                "int,int_with_gaps,float\n"
                "1,1,1.1\n"
                "2,,1.2\n"
                "3,3,1.3\n"
            )

        kwargs = {
            "datatypes": {
                "int": int,
                "int_with_gaps": int,
                "float": float
            },
            "obligatory_columns": ["int"],
            "converters": {}
        }
        importer = CSVImporter(**kwargs)
        assert importer.datatypes["int"] == "Int64"
        assert importer.datatypes["int_with_gaps"] == "Int64"
        assert importer.datatypes["float"] == float
        df = importer.read_file(tmpfile.name)
        # Default is to convert nullable ints
        assert df["int"].dtype == "Int64"
        assert df["int_with_gaps"].dtype == "Int64"
        assert df["float"].dtype == float

        assert pd.isna(df["int_with_gaps"][1])

        # When not converting, empty fields raise errors ...
        importer_strict = CSVImporter(convert_int_to_nullable_int=False, **kwargs)
        assert importer_strict.datatypes["int"] == int
        assert importer_strict.datatypes["int_with_gaps"] == int
        assert importer_strict.datatypes["float"] == float
        with pytest.raises(DataInconsistencyError) as die:
            df = importer_strict.read_file(tmpfile.name)
        assert "Integer column has NA values in column 1" in str(die.value)

        # ... except when a nullable datatype is set manually beforehand
        kwargs["datatypes"]["int_with_gaps"] = "Int64"
        importer_strict = CSVImporter(convert_int_to_nullable_int=False, **kwargs)
        df = importer_strict.read_file(tmpfile.name)
        # Now only the one that has been specifically set to Int64 is nullable.
        assert df["int"].dtype == int
        assert df["int_with_gaps"].dtype == "Int64"
        assert df["float"].dtype == float

    def test_wrong_datatype_type_errors(self):
        """Test for
        https://gitlab.com/linkahead/linkahead-advanced-user-tools/-/issues/63:
        Highlight rows and columns in which type errors occur.

        """
        tmpfile = NamedTemporaryFile(delete=False, suffix=".csv")
        with open(tmpfile.name, 'w') as tmp:
            # Wrong types in row 2, columns 1 and 2, and row 4, column 2.
            tmp.write(
                "int,float\n"
                "1,2.3\n"
                "4.5,word\n"
                "0,1.2\n"
                "-12,12+3j\n"
            )
        kwargs = {
            "datatypes": {
                "int": int,
                "float": float,
                "not-in-table": str  # An unused datatype definition must not cause problems.
            },
            "obligatory_columns": ["int"],
            "converters": {}
        }
        importer = CSVImporter(**kwargs)
        with pytest.raises(DataInconsistencyError) as die:
            df = importer.read_file(tmpfile.name)
        msg = str(die.value)
        print("\n" + msg)
        assert "Elements with wrong datatypes encountered:\n" in msg
        # Errors in rows 1 and 3, no errors in 2 and 4
        assert "* row 1:\n" in msg
        assert "* row 2:\n" not in msg
        assert "* row 3:\n" in msg
        assert "* row 4:\n" not in msg
        row_1_msgs, row_3_msgs = msg.split("* row 1:\n")[1].split("* row 3:\n")
        # exactly 2 errors in row 1, exactly 1 in row 3
        assert len(row_1_msgs.strip().split('\n')) == 2
        assert len(row_3_msgs.strip().split('\n')) == 1
        assert "  * column \"int\"" in row_1_msgs
        assert "  * column \"float\"" in row_1_msgs
        assert "  * column \"float\"" in row_3_msgs


class TSVImporterTest(TableImporterTest):
    def test_full(self):
        """ test full run with example data """
        tmp = NamedTemporaryFile(delete=False, suffix=".tsv")
        tmp.close()
        self.valid_df.to_csv(tmp.name, sep="\t")
        importer = TSVImporter(**self.importer_kwargs)
        importer.read_file(tmp.name)


class CountQueryNoneConverterTest(BaseMockUpTest):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # simulate that 0 entity exists
        self.entities = (
            '<Response count="0">'
            '<Query string="count record" results="0">'
            '</Query>'
            '</Response>'
        )

    def test_check_reference_field(self):
        self.assertRaises(ValueError, check_reference_field, "1232",  "Max")


class CountQuerySingleConverterTest(BaseMockUpTest):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # simulate that 1 entity exists
        self.entities = (
            '<Response count="1">'
            '<Query string="count record" results="1">'
            '</Query>'
            '</Response>'
        )

    def test_check_reference_field(self):
        self.assertEqual(check_reference_field("1232",  "Max"),
                         "1232")
