---
responsible: Ana Lytic
description: An examplary analysis of very exciting research. The analysis was conducted following state of the art best practices of scientific methodology.
sources: 
  - /ExperimentalData/2010_TestProject/2019-02-03_something/
  - file: /ExperimentalData/2010_TestProject/2019-02-03_something/
    description: An example reference to an experiment. The experimental data was analysed with statistical methods using proper error calculations.
scripts: 
  - file: scripts
    description: all the files needed to run the analysis
results: 
  - file: results.pdf
    description: a plot of the statistical analysis
...
