#!/usr/bin/env python3

# Copyright (C) 2023 IndiScale GmbH <www.indiscale.com>
# Copyright (C) 2023 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Create JSON-Schema according to configuration.

"""

from __future__ import annotations

import argparse
import json

import caosadvancedtools.json_schema_exporter as jsex
from caosadvancedtools.models import parser
# import tomli


def prepare_datamodel(modelfile, recordtypes: list[str], outfile: str,
                      do_not_create: list[str] = None):
    if do_not_create is None:
        do_not_create = []
    model = parser.parse_model_from_yaml(modelfile)

    exporter = jsex.JsonSchemaExporter(additional_properties=False,
                                       # additional_options_for_text_props=additional_text_options,
                                       # name_and_description_in_properties=True,
                                       name_property_for_new_records=True,
                                       do_not_create=do_not_create,
                                       # do_not_retrieve=do_not_retrieve,
                                       no_remote=True,
                                       use_rt_pool=model,
                                       )
    schemas = []
    for recordtype in recordtypes:
        schemas.append(exporter.recordtype_to_json_schema(model.get_deep(recordtype)))
    merged_schema = jsex.merge_schemas(schemas)

    with open(outfile, mode="w", encoding="utf8") as json_file:
        json.dump(merged_schema, json_file, ensure_ascii=False, indent=2)


def _parse_arguments():
    """Parse the arguments."""
    arg_parser = argparse.ArgumentParser(description='')

    return arg_parser.parse_args()


def main():
    """The main function of this script."""
    _ = _parse_arguments()
    prepare_datamodel("data/simple_model.yml", ["Training", "Person"], "data/simple_schema.json",
                      do_not_create=["Organisation"])
    prepare_datamodel("data/multiple_refs_model.yml", ["Training", "Person"],
                      "data/multiple_refs_schema.json")
    prepare_datamodel("data/indirect_model.yml", ["Wrapper"],
                      "data/indirect_schema.json")


if __name__ == "__main__":
    main()
