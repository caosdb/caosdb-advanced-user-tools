Insert the data model into a LinkAhead server.

Run the following code:
```
    model = parser.parse_model_from_yaml("./model.yml")

    exporter = jsex.JsonSchemaExporter(additional_properties=False,
                                       #additional_options_for_text_props=additional_text_options,
                                       #name_and_description_in_properties=True,
                                       #do_not_create=do_not_create,
                                       #do_not_retrieve=do_not_retrieve,
                                       )
    schema_top = exporter.recordtype_to_json_schema(model.get_deep("Training"))
    schema_pers = exporter.recordtype_to_json_schema(model.get_deep("Person"))
    merged_schema = jsex.merge_schemas([schema_top, schema_pers])

    with open("model_schema.json", mode="w", encoding="utf8") as json_file:
        json.dump(merged_schema, json_file, ensure_ascii=False, indent=2)
```
