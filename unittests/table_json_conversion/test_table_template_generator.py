#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2024 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json
import os
import tempfile

import pytest
from caosadvancedtools.table_json_conversion.table_generator import XLSXTemplateGenerator
from caosadvancedtools.table_json_conversion.xlsx_utils import ColumnType
from openpyxl import load_workbook

from .utils import compare_workbooks


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return os.path.join(os.path.dirname(__file__), *pathcomponents)


def _compare_generated_to_known_good(schema_file: str, known_good: str, foreign_keys: dict = None,
                                     outfile: str = None) -> tuple:
    """Generate an XLSX from the schema, then compare to known good output.

Returns
-------
out: tuple
  The generated and known good workbook objects.
    """
    generator = XLSXTemplateGenerator()
    if foreign_keys is None:
        foreign_keys = {}
    with open(schema_file, encoding="utf-8") as schema_input:
        schema = json.load(schema_input)
    with tempfile.TemporaryDirectory() as tmpdir:
        if outfile is None:
            outpath = os.path.join(tmpdir, 'generated.xlsx')
        else:
            outpath = outfile
        assert not os.path.exists(outpath)
        generator.generate(schema=schema,
                           foreign_keys=foreign_keys,
                           filepath=outpath)
        assert os.path.exists(outpath)
        generated = load_workbook(outpath)
    good = load_workbook(known_good)
    compare_workbooks(generated, good)
    return generated, good


def test_generate_sheets_from_schema():
    # trivial case; we do not support this
    schema = {}
    generator = XLSXTemplateGenerator()
    with pytest.raises(ValueError, match="Inappropriate JSON schema:.*"):
        generator._generate_sheets_from_schema(schema)

    # top level must be RT with Properties
    schema = {
        "type": "string"
    }
    with pytest.raises(ValueError, match="Inappropriate JSON schema:.*"):
        generator._generate_sheets_from_schema(schema)

    # bad type
    schema = {
        "type": "object",
        "properties": {
            "Training": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "str",
                        "description": "The name of the Record to be created"
                    },
                }
            }
        }
    }
    with pytest.raises(ValueError,
                       match="Inappropriate JSON schema: The following part "
                       "should define an object.*"):
        generator._generate_sheets_from_schema(schema, {'Training': ['a']})

    # bad schema
    schema = {
        "type": "object",
        "properties": {
            "Training": {
                "type": "object"
            }
        }
    }
    with pytest.raises(ValueError,
                       match="Inappropriate JSON schema: The following part "
                       "should define an object.*"):
        generator._generate_sheets_from_schema(schema, {'Training': ['a']})

    # minimal case: one RT with one P
    schema = {
        "type": "object",
        "properties": {
            "Training": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The name of the Record to be created"
                    },
                }
            }
        }
    }
    sdef = generator._generate_sheets_from_schema(schema, {'Training': ['a']})
    assert "Training" in sdef
    tdef = sdef['Training']
    assert 'name' in tdef
    assert tdef['name'] == (ColumnType.SCALAR, "The name of the Record to be created", ["Training", 'name'])

    # example case
    with open(rfp("data/simple_schema.json")) as sfi:
        schema = json.load(sfi)
    with pytest.raises(ValueError, match="A foreign key definition is missing.*"):
        generator._generate_sheets_from_schema(schema)
    sdef = generator._generate_sheets_from_schema(
        schema,
        foreign_keys={'Training': {"__this__": ['date', 'url']}})
    assert "Training" in sdef
    tdef = sdef['Training']
    assert tdef['date'] == (ColumnType.SCALAR, 'The date of the training.', ["Training", 'date'])
    assert tdef['url'] == (ColumnType.SCALAR, 'The URL', ["Training", 'url'])
    assert tdef['supervisor.family_name'] == (ColumnType.SCALAR, None, ["Training", 'supervisor',
                                                                        'family_name'])
    assert tdef['supervisor.given_name'] == (ColumnType.SCALAR, None, ["Training", 'supervisor',
                                                                       'given_name'])
    assert tdef['supervisor.Organisation'] == (ColumnType.SCALAR, None, ["Training", 'supervisor',
                                                                         'Organisation'])
    assert tdef['duration'] == (ColumnType.SCALAR, None, ["Training", 'duration'])
    assert tdef['participants'] == (ColumnType.SCALAR, None, ["Training", 'participants'])
    assert tdef['subjects'] == (ColumnType.LIST, None, ["Training", 'subjects'])
    assert tdef['remote'] == (ColumnType.SCALAR, None, ["Training", 'remote'])
    cdef = sdef['Training.coach']
    assert cdef['family_name'] == (ColumnType.SCALAR, None, ["Training", 'coach', 'family_name'])
    assert cdef['given_name'] == (ColumnType.SCALAR, None, ["Training", 'coach', 'given_name'])
    assert cdef['Organisation'] == (ColumnType.SCALAR, None, ["Training", 'coach',
                                                              'Organisation'])
    assert cdef['Training.date'] == (ColumnType.FOREIGN, "see sheet 'Training'", ["Training", 'date'])
    assert cdef['Training.url'] == (ColumnType.FOREIGN, "see sheet 'Training'", ["Training", 'url'])


def test_get_foreign_keys():
    generator = XLSXTemplateGenerator()
    fkd = {"Training": ['a']}
    assert [['a']] == generator._get_foreign_keys(fkd, ['Training'])

    fkd = {"Training": {"__this__": ['a']}}
    assert [['a']] == generator._get_foreign_keys(fkd, ['Training'])

    fkd = {"Training": {'hallo'}}
    with pytest.raises(ValueError, match=r"A foreign key definition is missing for path:\n\["
                       r"'Training'\]\nKeys are:\n{'Training': \{'hallo'\}\}"):
        generator._get_foreign_keys(fkd, ['Training'])

    fkd = {"Training": {"__this__": ['a'], 'b': ['c']}}
    assert [['c']] == generator._get_foreign_keys(fkd, ['Training', 'b'])

    with pytest.raises(ValueError, match=r"A foreign key definition is missing for .*"):
        generator._get_foreign_keys({}, ['Training'])


def test_get_max_path_length():
    assert 4 == XLSXTemplateGenerator._get_max_path_length({'a': (1, 'desc', [1, 2, 3]),
                                                            'b': (2, 'desc', [1, 2, 3, 4])})


def test_template_generator():
    generated, _ = _compare_generated_to_known_good(
        schema_file=rfp("data/simple_schema.json"), known_good=rfp("data/simple_template.xlsx"),
        foreign_keys={'Training': {"__this__": ['date', 'url']}},
        outfile=None)
    # test some hidden
    ws = generated.active
    assert ws.row_dimensions[1].hidden is True
    assert ws.column_dimensions['A'].hidden is True

    # TODO: remove the following after manual testing
    di = '/home/henrik/CaosDB/management/external/dimr/eingabemaske/crawler/schemas'
    if not os.path.exists(di):
        return
    for fi in os.listdir(di):
        rp = os.path.join(di, fi)
        if not fi.startswith("schema_"):
            continue
        with open(rp) as sfi:
            schema = json.load(sfi)
        fk_path = os.path.join(di, "foreign_keys"+fi[len('schema'):])
        path = os.path.join(di, "template"+fi[len('schema'):-4]+"xlsx")
        alreadydone = [
            "Präventionsmaßnahmen",
            "Beratungsstellen",
            "Schutzeinrichtungen",
            "Einzelfallversorgung",
            "Strategiedokumente",
            "Kooperationsvereinbarungen",
            "Gremien",
            "Verwaltungsvorschriften",
            "Gewaltschutzkonzepte und -maßnahmen",
            "Polizeilicher Opferschutz",
            "Feedback",
        ]
        if any([path.startswith("template_"+k) for k in alreadydone]):
            continue

        if not os.path.exists(fk_path):
            print(f"No foreign keys file for:\n{fk_path}")
            assert False
        with open(fk_path) as sfi:
            fk = json.load(sfi)
        generator = XLSXTemplateGenerator()
        if not os.path.exists(path):
            generator.generate(schema=schema, foreign_keys=fk, filepath=path)
            os.system(f'libreoffice {path}')
        else:
            print(f"Not creating template because it exists:\n{path}")

    # TODO test collisions of sheet or colnames
    # TODO test escaping of values

    # TODO finish enum example


def test_model_with_multiple_refs():
    _compare_generated_to_known_good(
        schema_file=rfp("data/multiple_refs_schema.json"),
        known_good=rfp("data/multiple_refs_template.xlsx"),
        foreign_keys={"Training": {"__this__": ["date", "url"],
                                   "Organisation": ["name"]}},
        outfile=None)


def test_model_with_indirect_reference():
    _compare_generated_to_known_good(
        schema_file=rfp("data/indirect_schema.json"),
        known_good=rfp("data/indirect_template.xlsx"),
        foreign_keys={"Wrapper": {"__this__": [["Training", "name"], ["Training", "url"]]}},
        outfile=None)


def test_model_with_multiple_choice():
    _compare_generated_to_known_good(
        schema_file=rfp("data/multiple_choice_schema.json"),
        known_good=rfp("data/multiple_choice_template.xlsx"),
        outfile=None)


def test_exceptions():
    # Foreign keys must be lists
    with pytest.raises(ValueError, match="Foreign keys must be a list of strings, but a single "
                       r"string was given:\n\['Wrapper'\] -> name"):
        _compare_generated_to_known_good(
            schema_file=rfp("data/indirect_schema.json"),
            known_good=rfp("data/multiple_refs_template.xlsx"),
            foreign_keys={"Wrapper": {"__this__": "name"}},
            outfile=None)
