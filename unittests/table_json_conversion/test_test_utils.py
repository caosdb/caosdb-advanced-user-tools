# encoding: utf-8
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""Testing the ``utils`` module in this folder."""


from .utils import _is_recursively_none


def test_recursively_none():
    """Testing ``_is_recursively_none``."""
    assert _is_recursively_none(None)
    assert _is_recursively_none([])
    assert _is_recursively_none({})
    assert _is_recursively_none([None])
    assert _is_recursively_none({"a": None})
    assert _is_recursively_none([[], [None, None]])
    assert _is_recursively_none({1: [], 2: [None], 3: {"3.1": None}, 4: {"4.1": [None]}})

    assert not _is_recursively_none(1)
    assert not _is_recursively_none([1])
    assert not _is_recursively_none({1: 2})
    assert not _is_recursively_none([[1]])
    assert not _is_recursively_none({"a": None, "b": "b"})
    assert not _is_recursively_none([[], [None, 2]])
    assert not _is_recursively_none({1: [], 2: [None], 3: {"3.1": 3.141}, 4: {"4.1": [None]}})
