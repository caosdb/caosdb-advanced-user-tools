#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the LinkAhead project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
module description
"""

import linkahead as db
from caosadvancedtools.serverside.generic_analysis import \
    check_referenced_script

from test_utils import BaseMockUpTest


class TestGAnalysisNoFile(BaseMockUpTest):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entities = (
            '<Response><Record name="script.py" path="/some/path/script.py'
            '" id="1234"/><Query string="find record" results="1">'
            '</Query></Response>')

    def test_check_referenced_script(self):
        # missing scripts
        self.assertIsNone(check_referenced_script(db.Record()))
        # wrong datatype
        self.assertIsNone(check_referenced_script(db.Record().add_property(
            "scripts", datatype=db.TEXT)))
        # wrong value
        self.assertIsNone(check_referenced_script(db.Record().add_property(
            "scripts", datatype=db.REFERENCE, value="hallo")))
        # no file
        self.assertIsNone(check_referenced_script(db.Record().add_property(
            "scripts", datatype=db.REFERENCE, value="1234")))


class TestGAnalysisFile(BaseMockUpTest):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.entities = (
            '<Response><File name="script.py" path="/some/path/script.py'
            '" id="1234"/><Query string="find record" results="1">'
            '</Query></Response>')

    def test_check_referenced_script(self):
        # all correct
        self.assertEqual(check_referenced_script(db.Record().add_property(
            "scripts", datatype=db.REFERENCE, value="1234")), "script.py")
