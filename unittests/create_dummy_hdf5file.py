import h5py
import numpy as np


def create_hdf5_file(filename="hdf5_dummy_file.hdf5"):
    '''
    Create a dummy hdf5-file for testing.
    Structure:

    root:-->root
        group_level1_a:-->group
            group_level2_aa:-->group
                group_level3_aaa:-->group
                    level3_aaa_floats_2d = float64(100x100)
                group_level3_aab:-->group
            group_level2_ab:-->group
                group_level3_aba:-->group
                    level3_aba_floats_2d = float64(100x100)
            group_level2_ac:-->group
                level2_ac_integers_2d = int32(100x100)
        group_level1_b:-->group
            group_level2_ba:-->group
                level2_ba_integers_2d = int32(100x100)
            level1_b_floats = float64(10000)
        group_level1_c:-->group
            level1_c_floats = float64(10000)
        root_integers = int32(10000)
    '''

    with h5py.File(filename, mode="w") as hdf5:
        '''Create toplevel groups'''
        group_lvl1_a = hdf5.create_group("group_level1_a")
        group_lvl1_b = hdf5.create_group("group_level1_b")
        group_lvl1_c = hdf5.create_group("group_level1_c")

        '''Create level 2 groups'''
        group_lvl2_aa = group_lvl1_a.create_group("group_level2_aa")
        group_lvl2_ab = group_lvl1_a.create_group("group_level2_ab")
        group_lvl2_ac = group_lvl1_a.create_group("group_level2_ac")
        group_lvl2_ba = group_lvl1_b.create_group("group_level2_ba")

        '''Create level 3 groups'''
        group_lvl3_aaa = group_lvl2_aa.create_group("group_level3_aaa")
        group_lvl3_aab = group_lvl2_aa.create_group("group_level3_aab")
        group_lvl3_aba = group_lvl2_ab.create_group("group_level3_aba")

        '''Create datasets'''
        integers = np.arange(10000)
        floats = np.arange(0, 1000, 0.1)
        integers_2d = np.diag(np.arange(100))
        floats_2d = np.eye(100)
        data_root = hdf5.create_dataset("root_integers", data=integers)
        data_lvl1_b = group_lvl1_b.create_dataset("level1_b_floats", data=floats)
        data_lvl2_c = group_lvl1_c.create_dataset("level1_c_floats", data=floats)
        data_lvl2_ac = group_lvl2_ac.create_dataset("level2_ac_integers_2d", data=integers_2d)
        data_lvl2_ba = group_lvl2_ba.create_dataset("level2_ba_integers_2d", data=integers_2d)
        data_lvl3_aaa = group_lvl3_aaa.create_dataset("level3_aaa_floats_2d", data=floats_2d)
        data_lvl3_aba = group_lvl3_aba.create_dataset("level3_aba_floats_2d", data=floats_2d)

        '''Create attributes'''
        attr_group_lvl1_a = group_lvl1_a.attrs.create("attr_group_lvl1_a", 1)
        attr_group_lvl2_aa = group_lvl2_aa.attrs.create("attr_group_lvl2_aa", -2)
        attr_group_lvl3_aaa = group_lvl3_aaa.attrs.create("attr_group_lvl3_aaa", 1.0)
        attr_data_root = data_root.attrs.create("attr_data_root", -2.0)
        attr_data_lvl2_ac = data_lvl2_ac.attrs.create("attr_data_lvl2_ac", np.diag(np.arange(10)))
        attr_data_lvl3_aaa = data_lvl3_aaa.attrs.create("attr_data_lvl3_aaa", np.eye(10))


if __name__ == "__main__":
    create_hdf5_file()
