#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the LinkAhead project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""Example update script. An anlysis Record is retrieved and passed to the
generic run function which then calls the appropriate script based on the
Record.

The simple query here could be replaced with something that e.g. retrieves all
entities that where changed within a certain period of time.

"""

import sys

import linkahead as db
from caosadvancedtools.serverside.generic_analysis import run


def main():
    da = db.execute_query("FIND ENTITY Analysis with identifier=TEST", unique=True)
    run(da)


if __name__ == "__main__":
    sys.exit(main())
