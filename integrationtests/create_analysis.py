#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the LinkAhead project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
A small script that creates an Analysis Record that can be used for testing the
automated analysis pipeline.
"""

import sys
from datetime import datetime

import linkahead as db


def main():
    script = db.File(
        file="../src/caosadvancedtools/serverside/examples/example_script.py",
        path=("AutomatedAnalysis/scripts/"
              + str(datetime.now())+"example_script.py"),
    )
    script.insert()

    da = db.Record()
    da.add_parent("Analysis")
    da.add_property("scripts", value=[script], datatype=db.LIST(db.FILE))
    da.add_property("sources",
                    value=db.execute_query(
                        "FIND FILE which is stored at '**/timeseries.npy'",
                        unique=True),
                    )
    da.add_property("date", "2020-01-01")
    da.add_property("identifier", "TEST")
    only = db.execute_query(
                        "FIND RECORD Person WITH firstname=Only",
                        unique=True)
    only.add_property(db.Property("Email").retrieve().id, "only@example.com")
    only.update()
    da.add_property("responsible", only)
    da.insert()


if __name__ == "__main__":
    sys.exit(main())
