---
responsible:	
- First Person
- Second Person
description: 	A description of an example experiment.

results:
- file:	"*.npy"
  description:  time series

scripts:
- "parameters.p"
- "large_sim.py"

revisonOf:
- ../2019-02-03

sources:
- /ExperimentalData/2010_TestProject/2019-02-03/

tags:
- collagen
- time sweep
- frequency sweep
...
