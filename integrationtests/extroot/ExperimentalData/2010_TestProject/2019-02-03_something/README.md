---
responsible:	
- First Person
- Second Person
description: 	A description of an example experiment.

results:
- file:	"*.xlsx"
  description:  an example reference to a results file

tags:
- collagen
- time sweep
- frequency sweep
...
