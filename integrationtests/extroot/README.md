This directory is mounted into the LinkAhead docker container, to allow the
inclusion of external file systems.  For production use, please set the
`paths:extroot` option in the profile.
