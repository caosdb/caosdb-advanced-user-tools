#!/usr/bin/env python
# encoding: utf-8
#
# This file is a part of the LinkAhead project.
#
# Copyright (C) 2023 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2023 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.
#

import json

import linkahead as db

from caosadvancedtools.json_schema_exporter import recordtype_to_json_schema as rtjs
from caosadvancedtools.models.parser import parse_model_from_string


def _delete_everything():
    ents = db.execute_query("FIND ENTITY WITH ID > 99")
    if ents:
        ents.delete()


def setup_module():
    _delete_everything()


def teardown_module():
    _delete_everything()


def test_uniqueness_of_reference_types():
    parent_type = db.RecordType(name="ParentType").insert()
    int_prop = db.Property(name="IntegerProp", datatype=db.INTEGER).insert()
    sub_type = db.RecordType(name="SubType").add_parent(parent_type).add_property(
        int_prop, importance=db.RECOMMENDED).insert()
    referencing_type = db.RecordType(name="ReferencingType")
    referencing_type.add_property(int_prop, importance=db.OBLIGATORY)
    referencing_type.add_property(parent_type)
    referencing_type.insert()
    recA = db.Record(name="RecAParent").add_parent(parent_type).insert()
    recB = db.Record(name="RecBSub").add_parent(sub_type).insert()

    rt = db.execute_query(f"FIND RECORDTYPE WITH name='{referencing_type.name}'", unique=True)

    schema = rtjs(rt)
    assert schema["title"] == referencing_type.name
    assert schema["type"] == "object"
    assert len(schema["required"]) == 1
    assert "IntegerProp" in schema["required"]
    assert "IntegerProp" in schema["properties"]
    assert schema["properties"]["IntegerProp"]["type"] == "integer"
    assert parent_type.name in schema["properties"]
    assert "oneOf" in schema["properties"][parent_type.name]
    one_of = schema["properties"][parent_type.name]["oneOf"]
    assert len(one_of) == 2
    enum_index = 0
    if "enum" not in one_of[enum_index]:
        # As in unittests, we can't rely on the order of oneOf.
        enum_index = 1 - enum_index
    assert "enum" in one_of[enum_index]
    assert len(one_of[enum_index]["enum"]) == 2
    assert recA.name in one_of[enum_index]["enum"]
    assert recB.name in one_of[enum_index]["enum"]
    assert one_of[1 - enum_index]["type"] == "object"
    # No properties in parent_type
    assert len(one_of[1 - enum_index]["properties"]) == 0


def test_reference_property():
    model_string = """
RT1:
  description: Some recordtype
RT2:
  obligatory_properties:
    prop1:
      description: Some reference property
      datatype: RT1
    """
    model = parse_model_from_string(model_string)
    model.sync_data_model(noquestion=True)
    schema = rtjs(db.RecordType(name="RT2").retrieve())
    assert json.dumps(schema, indent=2) == """{
  "type": "object",
  "required": [
    "prop1"
  ],
  "additionalProperties": true,
  "title": "RT2",
  "properties": {
    "prop1": {
      "type": "object",
      "required": [],
      "additionalProperties": true,
      "description": "Some reference property",
      "title": "prop1",
      "properties": {}
    }
  },
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}"""
