# Getting started

## Installation

To install the advancedtools package, you can run:  
`pip install caosadvancedtools`

#### Additional dependencies

To test using tox, you also need to install tox:  
`pip install tox`  

To install dependencies used by optional functionality, the following pip extras 
keywords are defined:
- `test` for testing with pytest
- `doc` for building the documentation
- `dev` for code formatting
- `h5` for the h5-crawler
- `all` to install all optional dependencies

These extras can be installed using: `pip install .[KEYWORD]`  

A current list of the dependencies installed with this program as well as those installed with 
the keywords can be found in `setup.py`s `setup_package()` method, in the `metadata` dictionary
entries `install_requires` and `extras_require`.

## Run Unit Tests

- All tests: `tox`
- One specific test with tox: `tox -- unittests/test_myusecase.py -k expression`
- Using only pytest: `pytest unittests` or for running only one test 
  `pytest unittests/test_myusecase.py -k expression`

## Run Integration Tests Locally

1. Change directory to `integrationtests/`.
2. Mount `extroot` to the folder that will be used as extroot. E.g. `sudo mount
   -o bind extroot ../../caosdb-deploy/profiles/debug/paths/extroot` (or
   whatever path the extroot of the empty profile to be used is located at).
3. Start (or restart) an empty (!) CaosDB instance (with the mounted
   extroot). The database will be cleared during testing, so it's important to
   use an empty instance. Make sure your configuration for the python caosdb
   module is correct and allows to connect to the server.
4. Run `test.sh`. Note that this may modify content of the
   `integrationtest/extroot/` directory.

## Code Formatting

`make style`

## Documentation #
We use sphinx to create the documentation. Docstrings in the code should comply
with the Googly style (see link below).

Build documentation in `build/` with `make doc`.


### How to contribute ###

- [Google Style Python Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
- [Google Style Python Docstrings 2nd reference](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings)
- [References to other documentation](https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#role-external)
