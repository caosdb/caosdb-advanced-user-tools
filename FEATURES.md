# Features

## Stable
To be filled.

## Experimental

- `generic_analysis.py` allows to easily call scripts to perform analyses in
  server side scripting
-  Models parser can import from Json Schema files:
  `models.parser.parse_model_from_json_schema(...)`. See the documentation of
  `models.parser.JsonSchemaParser` for the limitations of the current
  implementation.
